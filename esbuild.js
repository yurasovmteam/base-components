const {build} = require('esbuild');
const sveltePlugin = require('esbuild-svelte');
const {sassPlugin} = require('esbuild-sass-plugin');
const pkg = require(`./package.json`);

const svelte = sveltePlugin({
	compileOptions:{
	  css: false
	}
  });

// Собираем IIFE-модуль
build({
  entryPoints: [ pkg.main ],
  outfile: 'dist/bundle.min.js',
  format: 'iife',
  bundle: true,
  minify: true,
  sourcemap: false,
  plugins: [ svelte, sassPlugin() ],
  globalName: pkg.componentName
})